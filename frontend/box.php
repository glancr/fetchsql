<?php

require '../../../config/glancrConfig.php';

$host  = urldecode(getConfigValue('fetchsql_host'));
$user  = urldecode(getConfigValue('fetchsql_user'));
$pw    = urldecode(getConfigValue('fetchsql_passwd'));
$query = urldecode(getConfigValue('fetchsql_query'));
$db    = urldecode(getConfigValue('fetchsql_db'));
$limit = urldecode(getConfigValue('fetchsql_limit'));
$icon  = urldecode(getConfigValue('fetchsql_icon'));

$conn = mysqli_connect($host, $user, $pw);

if (!$conn) {
    die('Es ist ein Fehler aufgetreten! --> ' . $conn->error);
}
if (!$conn->select_db($db)) {
    die('Es ist ein Fehler aufgetreten! --> ' . $conn->error);
}


    $conn->query("SET NAMES 'utf8'") or die($conn->error);
    $conn->query("SET CHARACTER SET 'utf8'") or die($conn->error);


if ($limit == "*") {
    $limit = "";
} else {
    $limit = "LIMIT " . $limit;
}



    $result = $conn->query("$query $limit");
    $num_rows = $result->num_rows;
if ($num_rows == 0) {
    echo "Keine Einträge gefunden.";
}
?>

<table id="fetchsql">
    <tbody>

<?php

$sql = "$query $limit";
$i   = 0;
$query = $conn->query($sql) or die($conn->error());

while ($fetch = $result->fetch_assoc()) {
    echo '<tr><td class="fetchsql_icon"><i class="fetchsql_icon fa ' . $icon . '"></i></td>';

    foreach ($fetch as $key => $value) {
        echo "<td>$value</td>";
    }
    echo '</tr>';

    $i++;
}

$result->free();
$conn->close();

?>

</tbody>
</table>
