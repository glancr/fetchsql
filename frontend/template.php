<?php

$fetchsql_refresh_rate = getConfigValue('fetchsql_refresh');
if (empty($fetchsql_refresh_rate)) {
    $fetchsql_refresh_rate = 1;
}
?>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">



    <h2 class="module__title">
        <?php echo getConfigValue('fetchsql_title'); ?> 
    </h2>
    <script type="text/javascript">var fetchsql_refresh_rate = <?php echo $fetchsql_refresh_rate ?></script>
    <span id="fetchsql_content"></span>
