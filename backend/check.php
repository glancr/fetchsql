<?php

$host = $_GET['host'];
$user = $_GET['user'];
$passwd = $_GET['passwd'];
$db = $_GET['db'];
$query = $_GET['query'];
$passwd = str_replace("♣", "&", $passwd);

$conn = mysqli_connect($host, $user, $passwd);

if (!$conn) {
    $echo = "0";
}
if (!$conn->select_db($db)) {
    $echo = "0";
}

$result = $conn->query($query);
if (!$result) {
    $echo = "0";
} else {
    $echo = "1";
}

echo $echo;
