<?php

_("fetchsql_title");
_("fetchsql_description");
_("fetchsql_noentry");
?>

<script>


    $(document).ready(function () {
        
        var query = '<?php echo urldecode(getConfigValue('fetchsql_query')); ?>';
        $("#fetchsql_query").val(query);
        
        var fetchsql_title = '<?php echo urldecode(getConfigValue('fetchsql_title')); ?>';
        $("#fetchsql_title").val(fetchsql_title);
        
        var fetchsql_host = '<?php echo urldecode(getConfigValue('fetchsql_host')); ?>';
        $("#fetchsql_host").val(fetchsql_host);
        
        var fetchsql_user = '<?php echo urldecode(getConfigValue('fetchsql_user')); ?>';
        $("#fetchsql_user").val(fetchsql_user);
        
        var fetchsql_passwd = '<?php echo urldecode(getConfigValue('fetchsql_passwd')); ?>';
        $("#fetchsql_passwd").val(fetchsql_passwd);
        
        var fetchsql_db = '<?php echo urldecode(getConfigValue('fetchsql_db')); ?>';
        $("#fetchsql_db").val(fetchsql_db);
        
        var fetchsql_limit = '<?php echo urldecode(getConfigValue('fetchsql_limit')); ?>';
        $("#fetchsql_limit").val(fetchsql_limit);

        var fetchsql_refresh = '<?php echo urldecode(getConfigValue('fetchsql_refresh')); ?>';
        $("#fetchsql_refresh").val(fetchsql_refresh);
        
        var fetchsql_icon = '<?php echo urldecode(getConfigValue('fetchsql_icon')); ?>';
        $("#fetchsql_icon").val(fetchsql_icon);
    });
    
    
</script>


    <form action="javascript:void" id="fetch_settings">

        <input type="text" class="fetchsql_input" id="fetchsql_title" placeholder="<?php echo _('fetchsql_titletxt'); ?>" value=" " required />
        <input type="text" class="fetchsql_input" id="fetchsql_host" placeholder="<?php echo _('fetchsql_hosttxt'); ?>" value=" "  required/>
        <input type="text" class="fetchsql_input" id="fetchsql_user" placeholder="<?php echo _('fetchsql_usertxt'); ?>" value=" " required />
        <input type="text" class="fetchsql_input" id="fetchsql_passwd" placeholder="<?php echo _('fetchsql_pwtxt'); ?>" value=" " required />
        <input type="text" class="fetchsql_input" id="fetchsql_db" placeholder="<?php echo _('fetchsql_dbtxt'); ?>" value=" " required />
        <input type="text" class="fetchsql_input" id="fetchsql_query" placeholder="<?php echo _('fetchsql_querytxt'); ?>" value=" " required />
        <input type="text" class="fetchsql_input" id="fetchsql_refresh" placeholder="<?php echo _('fetchsql_refreshtxt'); ?>" value=" " required />
        <input type="text" class="fetchsql_input" id="fetchsql_limit" placeholder="<?php echo _('fetchsql_limittxt'); ?>" value=" " required />

        <input style="margin: 0px;" type="text" class="fetchsql_input" id="fetchsql_icon" placeholder="Font Awesome Icon" value=" " required />
        <a style="float: right; color: grey" target="_blank" h href="http://fontawesome.io/icons/">Icons?</a>

    </form>

   
   
    <div class="fetchsql_frame" id="fetchsql_error">
      <div class="fetchsql_modal">
        <img src="/modules/fetchsql/assets/error.png" width="44" height="38" />
            <span class="title"><?php echo _('fetchsql_errormsg'); ?></span>
      </div>
    </div>
    
    
    <hr>
    
    
    
    <div class="block__add" id="fetchsql_save" style="display:none" >
        <button  href="javascript:void">
            <span><?php echo _('fetchsql_savebtn'); ?></span>
        </button>
    </div>

    
    <div class="block__add" id="fetchsql_check"  >
        <button href="javascript:void">
            <span><?php echo _('fetchsql_checkbtn'); ?></span>
        </button>
    </div>



