$('#fetchsql_save').click(function() {
    var query_val = $("#fetchsql_query").val();
    var query = encodeURI(query_val);
	$.post('/config/setConfigValueAjax.php', {'key' : 'fetchsql_host', 'value': $("#fetchsql_host").val()});
	$.post('/config/setConfigValueAjax.php', {'key' : 'fetchsql_user', 'value': $("#fetchsql_user").val()});
	$.post('/config/setConfigValueAjax.php', {'key' : 'fetchsql_passwd', 'value': $("#fetchsql_passwd").val()});
	$.post('/config/setConfigValueAjax.php', {'key' : 'fetchsql_query', 'value': query });
	$.post('/config/setConfigValueAjax.php', {'key' : 'fetchsql_title', 'value': $("#fetchsql_title").val()});
	$.post('/config/setConfigValueAjax.php', {'key' : 'fetchsql_icon', 'value': $("#fetchsql_icon").val()});
    $.post('/config/setConfigValueAjax.php', {'key' : 'fetchsql_db', 'value': $("#fetchsql_db").val()});
    $.post("/config/setConfigValueAjax.php", {
      key: "fetchsql_refresh",
      value: $("#fetchsql_refresh").val()
    });
	$.post('/config/setConfigValueAjax.php', {'key' : 'fetchsql_limit', 'value': $("#fetchsql_limit").val()}).done(function() {
		$('#ok').show(30, function() {
			$(this).hide('slow');
		})

			location.reload();
	});
});

    $( "#fetch_settings" ).change(function() {
        $('#fetchsql_error').slideUp();
        $('#fetchsql_save').hide();
        $('#fetchsql_check').show();
    });



    $('#fetchsql_check').click(function() {
        
        
        var str = $("#fetchsql_title").val();
        var res = str.replace(";", "");
        $("#fetchsql_title").val(res);
        
        var str = $("#fetchsql_host").val();
        var res = str.replace(";", "");
        $("#fetchsql_host").val(res);
        
        
        var str = $("#fetchsql_db").val();
        var res = str.replace(";", "");
        $("#fetchsql_db").val(res);
        
        var str = $("#fetchsql_query").val();
        var res = str.replace(";", "");
        $("#fetchsql_query").val(res);
        
        var str = $("#fetchsql_limit").val();
        var res = str.replace(";", "");
        $("#fetchsql_limit").val(res);
        
        var str = $("#fetchsql_icon").val();
        var res = str.replace(";", "");
        $("#fetchsql_icon").val(res);
        
        
        var host = encodeURI($('#fetchsql_host').val());
        var user = encodeURI($('#fetchsql_user').val());
        var passwd = encodeURI($('#fetchsql_passwd').val());
        var db = encodeURI($('#fetchsql_db').val());
        var query = encodeURI($('#fetchsql_query').val());
        var title = encodeURI($('#fetchsql_title').val());
        var limit = encodeURI($('#fetchsql_limit').val());
        var passwd = passwd.replace(/&/g,"♣");

                    $.get('../modules/fetchsql/backend/check.php?host=' + host + '&user=' + user + '&passwd=' + passwd + '&db=' + db + '&query=' + query, function (data) {

                        if(data == "1"){
                            $('#fetchsql_check').hide();
                            $('#fetchsql_save').show();
                            $('#fetchsql_error').slideUp();
                        }
                        else{
                            $('#fetchsql_save').hide();
                            $('#fetchsql_check').show();
                            $('#fetchsql_error').slideDown();
                        }

                    });
    });




     

    $('#fetchsql_limit').keyup(function(e)
                                    {
      if (/\D/g.test(this.value))
      {
        // Filter non-digits from input value.
        this.value = this.value.replace(/\D/g, '');
      }
    });
    


